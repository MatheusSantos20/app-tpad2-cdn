Assunto: Sistema educacional
Tema: Educação pública
Delimitação: Evasão entre os jovens de 18 a 24 anos nos cursos de graduação no
ensino superior brasileiro.
Problema: A dificuldade de manter o jovem estudante na graduação.

Revisão da literatura: Segundo dados da Pesquisa Nacional por Amostra de Domicílios
(PNAD) em 2019, entre os jovens de 18 a 24 anos, aproximadamente 53%
estavam inseridos no mercado de trabalho, no qual 39% só trabalham e 14%
estudam e trabalham. De acordo com os Dados de uma pesquisa realizada por Adachi (2009)
sobre a evasão nos cursos de graduação na Universidade Federal de Minas
Gerais (UFMG) reforçam a dificuldade que os jovens sentem de conciliar
os estudos com o trabalho. Nesse estudo, constatou-se que 80% dos casos
de evasão na referida universidade ocorreram com alunos que trabalhavam
concomitantemente aos estudos. Ainda se verificou que, do total dos estu-
dantes não evadidos, apenas 30% trabalhavam, sendo que 70% eram apenas
estudantes. (de Medeiros Rosa and Ribeiro, 2017, p.10)

Objetivo geral: Compreender os comportamentos e as consequências causadas pela
alta evasão dos jovens cidadãos nos cursos de graduação no ensino
superior brasileiro

Objetivos específicos: Analisar as tomadas de decisões dos jovens de 18 a 24 anos
entre trabalha e estudar, nos quais fazem os jovens escolherem o
trabalho como prioridade. Mensurar o impacto da evasão por área
e o seu respectivo retorno para sociedade.

Justificativa: No cenário atual, a evasão de alunos nas graduações públicas crescem a cada ano que passa, pesquisas e estudos sugerem diversos fatores para essa ocorrência que acaba sendo um custo para a sociedade como um todo. Dessa forma, é necessário entender mais a fundo esses fatores e como reduzir o número de evasão nos cursos de graduação no ensino superior brasileiro para os impostos não serem em vão.